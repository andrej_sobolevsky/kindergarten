/*
 Задан массив  целых чисел. Определить максимальную длину 
 (+ позиции начала и конца) последовательности идущих подряд различных элементов.
 */
const SIZEARR = 20;
var arrNumbers = createArrNumbers(SIZEARR);

function createArrNumbers(size) {
    var arr = [];
    for (var i = 0; i < size; i++) {
        arr.push(Math.floor(Math.random() * 10));
    }
    return arr;
}

function seekMaxSequence(arr) {

    var result = {
        maxLength: 0
    };
    var _result = {
        arr: [],
        startPos: 0
    };

    function hasNumber(arr, number) {
        return arr.some(function (elem) {
            return elem === number;
        });
    }

    var i = 0;
    var maxLength = 0;

    while (i < (SIZEARR)) {

        if (!hasNumber(_result.arr, arr[i])) {
            _result.arr.push(arr[i]);
            i++;
            maxLength = i - _result.startPos;
        } else {
            if (maxLength > result.maxLength) {
                result.startPos = _result.startPos;
                result.endPos = i - 1;
                result.maxLength = maxLength;
            }
            _result.startPos = (arr.indexOf(arr[i], _result.startPos)) + 1;
            i = _result.startPos;
            _result.arr = [];
        }

        if ((i === (SIZEARR - 1)) && (maxLength > result.maxLength)) {
            result.startPos = _result.startPos;
            result.endPos = i;
            result.maxLength = maxLength + 1;
        }
    }
    return result;
}

console.log(arrNumbers);
console.log(seekMaxSequence(arrNumbers));