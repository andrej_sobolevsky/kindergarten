/*
 Разложить книги по 2-м стопкам так, чтобы разница высот была минимальной.
 */

var books = [10, 30, 20,45,45,56,800, 67,78,89,88,89,56,32,545,56,64];

function divBooks(books) {
    
    var piles = {
        first: [],
        second: []
    };
    
    function heightBooks(pile) {
        return pile.reduce(function (prev, next) {
            return prev + next;
        }, 0);
    }
    
    books.sort(function (a,b) {
        return a - b;
    });
    
    books.reverse().forEach(function (book) {
        if (heightBooks(piles.first) <= heightBooks(piles.second)){
            piles.first.push(book)
        }else piles.second.push(book)
    });
    
    piles.diff = Math.abs(heightBooks(piles.first) - heightBooks(piles.second));
    return piles;
}

console.log(divBooks(books));