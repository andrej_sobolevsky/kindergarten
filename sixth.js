/*Последовательность 1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 1, 0, …,
 состоящая из 0 и 1 строиться так: первый ее элемент 1, а
 остальные получаются из предыдущих с помощью логического
 отрицания not(1) = 0, not(0) = 1. Второй элемент равен отрицанию
 первого, третий и четвертый – отрицанием 1-го и 2-го соответственно и т.д.
 Вычислить N-й член описанной 	последовательности по заданному N.
 */

function valueOf(n) {
    var arr = [1];
    function fillArray(oldArr){
        return oldArr.concat(oldArr.map(function (number) {
            return Number(!number);
        }))
    }
    var length = 1;
    while (length <= n){
        arr = fillArray(arr);
        length = arr.length;
    }
    return arr[n-1];
}
console.log(valueOf(10));


