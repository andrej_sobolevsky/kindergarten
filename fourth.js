/*
 Оптимальная расстановка скобок. В арифметическом 	выражении, операндами
 которого являются целые числа от 0 до 9, а операциями – бинарные
 операции «+» и «*», необходимо расставить скобки так, чтобы результат
 оказался максимальным из всех возможных.
 */
function changeExpression(str) {
    var arr = str.split('');
    var open = false;
    var result = '';
    
    while (arr.length > 1) {
        if (arr[1] === '+' && !open) {
            result = result + '(' + arr.shift() + arr.shift();
            open = true;
        } else {
            if (arr[1] === '*' && open) {
                result = result + arr.shift() + ')' + arr.shift();
                open = false;
            }else result = result + arr.shift() + arr.shift();
        }
    }
    if (open){
        return result + arr.shift() + ')'
    }else return result + arr.shift()
}

console.log(changeExpression());