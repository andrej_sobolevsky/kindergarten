/*
 Есть строка, 	состоящая из слов. Необходимо реализовать наиболее
 эффективный алгоритм перестановки слов в обратном порядке 
 в предложении (без выделения дополнительной памяти).
 */

function replaceStr(string){
    return string.split(' ').reduce(function (previous, current) {
        return current + ' ' + previous;
    });
}
console.log(replaceStr());